#include "processing.h"
#include <iostream>

int time(date d) {
	int result = 0;
	result = d.hour * 3600 + d.minute * 60 + d.second;
	return result;
}

int process(marathon_results* array[], int size)
{
	int min = time(array[0]->finish);
	for (int i = 1; i < size; i++)
	{
		int curr = time(array[i]->finish);
		if (curr < min)
		{
			min = curr;
		}
	}
	
	std::cout << min / 3600 << ":" << (min % 3600) / 60 << ":" << min % 60 << '\n';

	return 0;
}