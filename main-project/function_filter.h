#pragma once
#ifndef FUNCTION_FILTER_H
#define FUNCTION_FILTER_H

#include "MARATHON_RESULTS.h"

marathon_results** filter(marathon_results* array[], int size, bool (*check)(marathon_results* element), int& result_size);


bool check_club(marathon_results* element);

bool check_time(marathon_results* element);

#endif