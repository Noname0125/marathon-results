#ifndef MARATHON_RESULTS_H
#define MARATHON_RESULTS_H

#include "constants.h"

struct date
{
    int hour;
    int minute;
    int second;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct marathon_results
{
    int number;
    person participant;
    date start;
    date finish;
    char club[MAX_STRING_SIZE];
};

#endif