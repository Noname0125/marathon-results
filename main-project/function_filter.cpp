#include "function_filter.h"
#include <cstring>
#include <iostream>
marathon_results** filter(marathon_results* array[], int size, bool (*check)(marathon_results* element), int& result_size)
{
	marathon_results** result = new marathon_results * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_club(marathon_results* element)
{
	return strcmp(element->club, "�������") == 0;
}

bool check_time(marathon_results* element)
{
	return element->finish.hour <= 12 && element->finish.minute <= 50;
}