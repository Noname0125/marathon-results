#include <iostream>
#include <iomanip>

using namespace std;

#include "MARATHON_RESULTS.h"
#include "file_reader.h"
#include "constants.h"
#include "function_filter.h"
#include "Processing.h"

void output(marathon_results* subscriptions)
{           /******* ����� ��������� *******/
            cout << "����� ���������: ";
            cout << subscriptions->number << '\n';
            cout << "��������.......: ";
            // ����� �������
            cout << subscriptions->participant.last_name << " ";
            // ����� ������ ����� �����
            cout << subscriptions->participant.first_name[0] << ". ";
            // ����� ������ ����� ��������
            cout << subscriptions->participant.middle_name[0] << ".";
            cout << '\n';
            /******* ����� �������� ������� *******/
            cout << "����...........: ";
            cout << '"' << subscriptions->club << '"';
            cout << '\n';
            /******* ����� ������� ������ *******/
            // ����� �����
            cout << "����� ������...: ";
            cout << setw(2) << setfill('0') << subscriptions->start.hour << ':';
            // ����� �����
            cout << setw(2) << setfill('0') << subscriptions->start.minute << ':';
            // ����� ������
            cout << setw(2) << setfill('0') << subscriptions->start.second;
            cout << '\n';
            /******* ����� ������� ������ *******/
            // ����� �����
            cout << "����� ������...: ";
            cout << setw(2) << setfill('0') << subscriptions->finish.hour << ':';
            // ����� �����
            cout << setw(2) << setfill('0') << subscriptions->finish.minute << ':';
            // ����� ������
            cout << setw(2) << setfill('0') << subscriptions->finish.second;
            cout << '\n';
            cout << '\n';
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �9. GIT\n";
    cout << "������� �1. ���������� ��������\n";
    cout << "�����: ���� ����������\n";
    cout << "������: 14\n\n";
    marathon_results* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** ���������� �������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }

        bool (*check_function)(marathon_results*) = NULL;
        cout << "\n�������� ������ ���������� ������:\n";
        cout << "1) ������� ������ ���������� ����� �������\n";
        cout << "2) ������� ������ ����������, ����� ������� �����, ��� 2 ���� 50 �����\n";
        cout << "3) ������� ������ ����� ������\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';
        int second;

        switch (item)
        {
          case 1:
            check_function = check_club; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ��������� ����� ������� *****\n\n";
            break;

          case 2:
            check_function = check_time;
            cout << "***** ����� ������ 2-� ����� 50 �����*****\n\n";
            break;

          case 3:
            cout << "***** ������ ����� *****\n\n";
            second = process(subscriptions,size);
            cout << second;
            break;

          default:
            throw "������������ ����� ������";
        }


        if (check_function)
        {
            int new_size;
            marathon_results** filtered = filter(subscriptions, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }

        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }

    }

    catch (const char* error)
    {
        cout << error << '\n';
        return 0;
    }
}