#ifndef PROCESSING_H
#define PROCESSING_H

#include "MARATHON_RESULTS.h"

int time(date d);

int process(marathon_results* array[], int size);

#endif