#ifndef FILE_READER_H
#define FILE_READER_H

#include "MARATHON_RESULTS.h"

void read(const char* file_name, marathon_results* array[], int& size);

#endif